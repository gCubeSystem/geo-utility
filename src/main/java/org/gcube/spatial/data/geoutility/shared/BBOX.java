package org.gcube.spatial.data.geoutility.shared;

import java.io.Serializable;

public class BBOX implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8703212808072493411L;

	/** The lower left X. */
	private double lowerLeftX = 0.0;

	/** The lower left Y. */
	private double lowerLeftY = 0.0;

	/** The upper right X. */
	private double upperRightX = 0.0;

	/** The upper right Y. */
	private double upperRightY = 0.0;

	/** The crs. */
	private String crs = "";

	public static enum COORDINATE_FORMAT {
		XY, YX
	}

	public static final String BBOX_Separator = ",";

	public BBOX() {
	}

	/**
	 * Instantiates a new bbox.
	 *
	 * @param bbox           the bbox as [minX,minY,maxX,maxY] XY (e.g. when service
	 *                       version is 1.1.0). The bbox as [minY,minX,maxY,maxX] YZ
	 *                       (e.g. when service version is 1.3.0)
	 * @param serviceVersion the service version
	 */
	public BBOX(String bbox, COORDINATE_FORMAT format) {

		if (format == null)
			format = COORDINATE_FORMAT.XY;

		String[] theBBOX = bbox.replace("\\[", "").replace("\\]", "").split(",");
		switch (format) {
		case XY:
			this.lowerLeftX = Double.parseDouble(theBBOX[0]);
			this.lowerLeftY = Double.parseDouble(theBBOX[1]);
			this.upperRightX = Double.parseDouble(theBBOX[2]);
			this.upperRightY = Double.parseDouble(theBBOX[3]);
			break;
		case YX:
			this.lowerLeftX = Double.parseDouble(theBBOX[1]);
			this.lowerLeftY = Double.parseDouble(theBBOX[0]);
			this.upperRightX = Double.parseDouble(theBBOX[2]);
			this.upperRightY = Double.parseDouble(theBBOX[3]);
			break;
		}
	}

	public BBOX(double lowerLeftX, double lowerLeftY, double upperRightX, double upperRightY, String crs) {
		super();
		this.lowerLeftX = lowerLeftX;
		this.lowerLeftY = lowerLeftY;
		this.upperRightX = upperRightX;
		this.upperRightY = upperRightY;
		this.crs = crs;
	}

	public String toBBOXString(COORDINATE_FORMAT format) {

		if (format == null)
			format = COORDINATE_FORMAT.XY;

		switch (format) {
		case XY:
			return String.format("%s%s%s%s%s%s%s", String.valueOf(lowerLeftX), BBOX_Separator,
					String.valueOf(lowerLeftY), BBOX_Separator, String.valueOf(upperRightX), BBOX_Separator,
					String.valueOf(upperRightY));
		case YX:
			return String.format("%s%s%s%s%s%s%s", String.valueOf(lowerLeftY), BBOX_Separator,
					String.valueOf(lowerLeftX), BBOX_Separator, String.valueOf(upperRightY), BBOX_Separator,
					String.valueOf(upperRightX));
		}

		return null;
	}

	public static String toBBOXString(BBOX extBBOX, COORDINATE_FORMAT format) {

		if (format == null)
			format = COORDINATE_FORMAT.XY;

		switch (format) {
		case XY:
			return String.format("%s%s%s%s%s%s%s", String.valueOf(extBBOX.getLowerLeftX()), BBOX_Separator,
					String.valueOf(extBBOX.getLowerLeftY()), BBOX_Separator, String.valueOf(extBBOX.getUpperRightX()),
					BBOX_Separator, String.valueOf(extBBOX.getUpperRightY()));
		case YX:
			return String.format("%s%s%s%s%s%s%s", String.valueOf(extBBOX.getLowerLeftY()), BBOX_Separator,
					String.valueOf(extBBOX.getLowerLeftX()), BBOX_Separator, String.valueOf(extBBOX.getUpperRightY()),
					BBOX_Separator, String.valueOf(extBBOX.getUpperRightX()));
		}

		return null;
	}

	/**
	 * Gets the lower left X.
	 *
	 * @return the lower left X
	 */
	public double getLowerLeftX() {
		return lowerLeftX;
	}

	/**
	 * Sets the lower left X.
	 *
	 * @param lowerLeftX the new lower left X
	 */
	public void setLowerLeftX(double lowerLeftX) {
		this.lowerLeftX = lowerLeftX;
	}

	/**
	 * Gets the lower left Y.
	 *
	 * @return the lower left Y
	 */
	public double getLowerLeftY() {
		return lowerLeftY;
	}

	/**
	 * Sets the lower left Y.
	 *
	 * @param lowerLeftY the new lower left Y
	 */
	public void setLowerLeftY(double lowerLeftY) {
		this.lowerLeftY = lowerLeftY;
	}

	/**
	 * Gets the upper right X.
	 *
	 * @return the upper right X
	 */
	public double getUpperRightX() {
		return upperRightX;
	}

	/**
	 * Sets the upper right X.
	 *
	 * @param upperRightX the new upper right X
	 */
	public void setUpperRightX(double upperRightX) {
		this.upperRightX = upperRightX;
	}

	/**
	 * Gets the upper right Y.
	 *
	 * @return the upper right Y
	 */
	public double getUpperRightY() {
		return upperRightY;
	}

	/**
	 * Sets the upper right Y.
	 *
	 * @param upperRightY the new upper right Y
	 */
	public void setUpperRightY(double upperRightY) {
		this.upperRightY = upperRightY;
	}

	/**
	 * Gets the crs.
	 *
	 * @return the crs
	 */
	public String getCrs() {
		return crs;
	}

	/**
	 * Sets the crs.
	 *
	 * @param crs the new crs
	 */
	public void setCrs(String crs) {
		this.crs = crs;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BBOX [lowerLeftX=");
		builder.append(lowerLeftX);
		builder.append(", lowerLeftY=");
		builder.append(lowerLeftY);
		builder.append(", upperRightX=");
		builder.append(upperRightX);
		builder.append(", upperRightY=");
		builder.append(upperRightY);
		builder.append(", crs=");
		builder.append(crs);
		builder.append("]");
		return builder.toString();
	}

}