package org.gcube.spatial.data.geoutility.wfs;

import java.util.HashMap;
import java.util.List;

import org.gcube.spatial.data.geoutility.shared.wfs.WFSParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class WFSQueryBuilder.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jan 28, 2022
 */
public class WFSQueryBuilder {

	private static Logger LOG = LoggerFactory.getLogger(WFSQueryBuilder.class);

	private String query = "";

	private static HashMap<WFSParameter, String> wfsGetFeatureRequest = new HashMap<WFSParameter, String>();

	public static final String GEOM_NAME_BOUNDED = "geom";

	/**
	 * Instantiates a new WFS query builder.
	 */
	public WFSQueryBuilder() {

	}

	/**
	 * Gets the default WFS get feature request.
	 *
	 * @return the default WFS get feature request
	 */
	public HashMap<WFSParameter, String> getDefaultWFSGetFeatureRequest() {

		wfsGetFeatureRequest.clear();

		for (WFSParameter key : WFSParameter.values()) {
			wfsGetFeatureRequest.put(key, key.getValue());
		}

		return wfsGetFeatureRequest;
	}

	/**
	 * Adds the parameter.
	 *
	 * @param param the param
	 * @param value the value
	 */
	public void addParameter(WFSParameter param, String value) {

		if (!query.isEmpty())
			query += "&";

		query += param + "=" + value;
	}

	/**
	 * Adds the parameter.
	 *
	 * @param param    the param
	 * @param values   the values
	 * @param separtor the separtor
	 */
	public void addParameter(WFSParameter param, List<String> values, String separtor) {

		if (!query.isEmpty())
			query += "&";

		String value = "";
		for (int i = 0; i < values.size() - 1; i++) {
			value += values.get(i) + separtor;
		}

		value += values.get(values.size() - 1);

		query += param + "=" + value;
	}

	/**
	 * Gets the query.
	 *
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * Builds the WFS feature query.
	 *
	 * @param endpoint             the endpoint
	 * @param wfsGetFeatureRequest the wfs get feature request
	 * @return the string
	 */
	public String buildWFSFeatureQuery(String endpoint, HashMap<WFSParameter, String> wfsGetFeatureRequest) {

		LOG.debug("Map server endpoint: " + endpoint);
		LOG.debug("wfsGetFeatureRequest is: " + wfsGetFeatureRequest);

		for (WFSParameter param : wfsGetFeatureRequest.keySet()) {
			if (param != null) {
				String value = wfsGetFeatureRequest.get(param);
				if (value != null) {
					switch (param) {
					case CQL_FILTER: {
						if (!value.isEmpty()) {
							if (value.contains("BBOX(" + GEOM_NAME_BOUNDED + "")) {
								// THE BBOX IS ALREADY USED INTO CQL FILTERING, SO USING IT DIRECTLY
								addParameter(WFSParameter.CQL_FILTER, value);
							} else {
								// I NEED TO ENCODE THE BBOX INTO CQL FILTERING,
								String boundingBox = wfsGetFeatureRequest.get(WFSParameter.BBOX);
								String cqlFilterValue = "BBOX(" + GEOM_NAME_BOUNDED + "," + boundingBox + ")" + " AND "
										+ value;
								addParameter(WFSParameter.CQL_FILTER, cqlFilterValue);
							}
						}
						break;
					}
					case BBOX:
						// skipping
						break;

					case SRSNAME: {
						// skipping
						break;
					}

					case MAXFEATURES: {
						// Adding MAXFEATURES only if is not empty and it is an integer
						try {
							if (!value.isEmpty()) {
								Integer.parseInt(value);
								addParameter(param, value);
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
						break;
					}

					default:
						if (!value.isEmpty())
							addParameter(param, value);
						break;
					}
				}
			}
		}

		// If the query does not contain the CRS adding the SRSNAME
		if (!query.contains(WFSParameter.CRS.getParameter())) {
			String srsName = wfsGetFeatureRequest.get(WFSParameter.SRSNAME);
			if (srsName != null)
				addParameter(WFSParameter.SRSNAME, srsName);
		}

		// If the query does not contain the CQL_FILTER adding the BBOX
		if (!query.contains(WFSParameter.CQL_FILTER.getParameter())) {
			String boundingBox = wfsGetFeatureRequest.get(WFSParameter.BBOX);
			if (boundingBox != null)
				addParameter(WFSParameter.BBOX, boundingBox);
		}

		String wfsQuery = endpoint + "?" + query;

		LOG.info("WFS: " + wfsQuery);
		return wfsQuery;
	}
}
