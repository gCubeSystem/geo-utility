/**
 *
 */
package org.gcube.spatial.data.geoutility.wfs;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.gcube.spatial.data.geoutility.shared.wfs.FeatureGeometry;
import org.gcube.spatial.data.geoutility.shared.wfs.FeatureRow;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class FeatureParser.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jan 28, 2022
 */
public class FeatureParser {

	private static Logger LOG = LoggerFactory.getLogger(FeatureParser.class);

	/**
	 * Gets the WFS features.
	 *
	 * @param wfsEndPoint the wfs end point
	 * @param wfsQB       the wfs QB
	 * @return the WFS features
	 */
	public static List<FeatureRow> getWFSFeatures(String wfsEndPoint, WFSQueryBuilder wfsQB) {

		return getWFSFeatureProperties(wfsEndPoint + "?" + wfsQB.getQuery());
	}

	/**
	 * Gets the WFS feature properties.
	 *
	 * @param wfsURLRequest the wfs URL request
	 * @return the WFS feature properties
	 */
	@SuppressWarnings("unchecked")
	private static List<FeatureRow> getWFSFeatureProperties(String wfsURLRequest) {
		LOG.info("getWFSFeatureProperties for url:  " + wfsURLRequest);
		InputStream is = null;
		List<FeatureRow> listFeaturesRow = new ArrayList<FeatureRow>();
		try {

			LOG.info("Built WFS URL:  " + wfsURLRequest);
			is = new URL(wfsURLRequest).openStream();
			String jsonTxt = IOUtils.toString(is);

			if (jsonTxt == null || jsonTxt.isEmpty()) {
				jsonTxt = "{\"type\":\"FeatureCollection\",\"features\":[]}";
			}

			// get json object
			JSONObject json = new JSONObject(jsonTxt);
			// iterate features
			JSONArray features = json.getJSONArray("features");
			if (features.length() == 0) {
				LOG.info("No features detected in the response, returning empty list");
				return listFeaturesRow;
			}

			String featureCRSName = "";
			try {
				JSONObject crs = json.getJSONObject("crs");
				JSONObject crsProp = crs.getJSONObject("properties");
				featureCRSName = crsProp.getString("name");
				LOG.info("Crs name found:  " + featureCRSName);
			} catch (Exception e) {
				LOG.warn("Unable to read the field 'crs'");
			}

			LOG.info("Features are:  " + features.length());

			for (int i = 0; i < features.length(); i++) {
				final FeatureRow row = new FeatureRow();
				row.setCrsName(featureCRSName);
				JSONObject theFeature = ((JSONObject) features.get(i));
				LOG.debug("Building at index: " + i);

				try {
					String fetaureId = theFeature.getString("id");
					row.setId(fetaureId);
					JSONObject geometry = theFeature.getJSONObject("geometry");
					String typeValue = geometry.getString("type");
					FeatureGeometry fg = new FeatureGeometry();
					fg.setType(typeValue);

					try {
						JSONArray coordinates = geometry.getJSONArray("coordinates");
						String coordinateJSONString = coordinates.toString();
						LOG.debug("coordinates are: " + coordinateJSONString);
						fg.setCoordinatesJSON(coordinates.toString());
					} catch (Exception e) {
						LOG.warn("Not able to parse the 'coordinates' field: ", e);
					}

					row.setGeometry(fg);
				} catch (Exception e) {
					LOG.debug("Unable to parse geometry at index: " + i);
				}

//				// iterate properties
				JSONObject properties = theFeature.getJSONObject("properties");
				Map<String, List<String>> mapProperties = new HashMap<String, List<String>>();
				@SuppressWarnings("unchecked")
				Iterator<String> ii = properties.keys();
				while (ii.hasNext()) {
					String key = ii.next();
					String value = properties.optString(key, "");

					List<String> theValues = mapProperties.get(key);
					if (theValues == null)
						mapProperties.put(key, Arrays.asList(value));
					else {
						theValues.add(value);
						mapProperties.put(key, theValues);
					}
				}
				row.setMapProperties(mapProperties);
				listFeaturesRow.add(row);
				LOG.info("Added row " + row + " to exported properties");
			}

		} catch (IOException e) {
			LOG.error("Error on requesting properties for url: " + wfsURLRequest, e);
		} catch (JSONException e) {
			LOG.error("Error on requesting properties for url: " + wfsURLRequest, e);
		} finally {
			IOUtils.closeQuietly(is);
		}

		LOG.info("Returning  " + listFeaturesRow.size() + " features");
		return listFeaturesRow;
	}
}
