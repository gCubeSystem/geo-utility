package org.gcube.spatial.data.geoutility.shared.wfs;


/**
 * The Enum WFSParameter with default value (parameter,value)
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jan 28, 2022
 */
public enum WFSParameter {

	SERVICE("SERVICE", "WFS"), 
	VERSION("VERSION", "1.1.0"), 
	REQUEST("REQUEST", "GetFeature"), 
	TYPENAME("TYPENAME", ""),
	STYLES("STYLES", ""), 
	BBOX("BBOX", "-180,-90,180,90"), 
	SRSNAME("srsName", "EPSG:4326"),
	CRS("CRS",""), //WMS 1.3.0 COMPLIANT
	OUTPUTFORMAT("OUTPUTFORMAT", "application/json"), 
	MAXFEATURES("MAXFEATURES", ""),
	PROPERTYNAME("PROPERTYNAME",""),
	CQL_FILTER("CQL_FILTER","");

	private String parameter;
	private String value;

	/**
	 * Instantiates a new wfs parameters.
	 *
	 * @param parameter the parameter
	 * @param value     the value
	 */
	WFSParameter(String parameter, String value) {
		this.parameter = parameter;
		this.value = value;
	}

	/**
	 * Gets the parameter.
	 *
	 * @return the parameter
	 */
	public String getParameter() {
		return parameter;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

}
