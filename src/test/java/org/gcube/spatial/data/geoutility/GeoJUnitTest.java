/**
 *
 */
package org.gcube.spatial.data.geoutility;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

//import org.gcube.spatial.data.geoutility.CustomerTable.Column;
import org.gcube.spatial.data.geoutility.shared.wfs.FeatureRow;
import org.gcube.spatial.data.geoutility.shared.wfs.WFSParameter;
import org.gcube.spatial.data.geoutility.wfs.FeatureParser;
import org.gcube.spatial.data.geoutility.wfs.WFSQueryBuilder;
import org.gcube.spatial.data.geoutility.wms.WmsUrlValidator;
import org.junit.Before;
import org.junit.Test;

//import com.google.common.collect.Table;

/**
 * The Class GeoJUnitTest.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jan 22, 2016
 */
public class GeoJUnitTest {

//	CustomerTable tableReg = new CustomerTable();
//	CustomerTable tableProv = new CustomerTable();

	/**
	 * Test get styles.
	 */
	// @Test
	public void testGetStyles() {
//		String wmsRequest = "http://repoigg.services.iit.cnr.it:8080/geoserver/IGG/ows?service=wms&version=1.1.0&request=GetMap&layers==IGG:area_temp_1000&width=676&height=330&srs=EPSG:4326&crs=EPSG:4326&format=application/openlayers&bbox=-85.5,-180.0,90.0,180.0";
//		String wmsRequest = "http://thredds-d-d4s.d4science.org/thredds/wms/public/netcdf/test20.nc?service=wms&version=1.3.0&request=GetMap&layers=analyzed_field&styles=&width=640&height=480&srs=EPSG:4326&CRS=EPSG:4326&format=image/png&COLORSCALERANGE=auto&bbox=-85.0,-180.0,85.0,180.0";
//		String wmsRequest = "http://www.fao.org/figis/geoserver/species?SERVICE=WMS&BBOX=-176.0,-90.0,180.0,90&styles=&layers=layerName&FORMAT=image/gif";
		String wmsRequest = "http://geoserver-dev.d4science-ii.research-infrastructures.eu/geoserver/wms?CRS=EPSG:4326&BBOX=-85.5,-180.0,90.0,180.0&VERSION=1.1.0&FORMAT=application/openlayers&SERVICE=wms&HEIGHT=330&LAYERS=aquamaps:lsoleasolea20130716162322254cest&REQUEST=GetMap&STYLES=Species_prob&SRS=EPSG:4326&WIDTH=676";
		GeoNcWMSMetadataUtility geo;
		try {
			geo = new GeoNcWMSMetadataUtility(wmsRequest);
			System.out.println("Returned styles: " + geo.loadStyles());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Test get styles.
	 */
//	@Test
	public void testLoadZAxis() {
//		String wmsRequest = "http://repoigg.services.iit.cnr.it:8080/geoserver/IGG/ows?service=wms&version=1.1.0&request=GetMap&layers==IGG:area_temp_1000&width=676&height=330&srs=EPSG:4326&crs=EPSG:4326&format=application/openlayers&bbox=-85.5,-180.0,90.0,180.0";
		String wmsRequest = "http://thredds-d-d4s.d4science.org/thredds/wms/public/netcdf/test20.nc?service=wms&version=1.3.0&request=GetMap&layers=analyzed_field&styles=&width=640&height=480&srs=EPSG:4326&CRS=EPSG:4326&format=image/png&COLORSCALERANGE=auto&bbox=-85.0,-180.0,85.0,180.0";
//		String wmsRequest = "http://www.fao.org/figis/geoserver/species?SERVICE=WMS&BBOX=-176.0,-90.0,180.0,90&styles=&layers=layerName&FORMAT=image/gif";
		GeoNcWMSMetadataUtility geo;
		try {
			geo = new GeoNcWMSMetadataUtility(wmsRequest);
			System.out.println("Returned Z-Axis: " + geo.loadZAxis());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Test wms url validator.
	 */
//	@Test
	public void testWMSUrlValidator() {
		String wmsRequest = "http://geoserver-dev.d4science-ii.research-infrastructures.eu/geoserver/wms?CRS=EPSG:4326&BBOX=-85.5,-180.0,90.0,180.0&VERSION=1.1.0&FORMAT=application/openlayers&SERVICE=wms&HEIGHT=330&LAYERS=aquamaps:lsoleasolea20130716162322254cest&REQUEST=GetMap&STYLES=Species_prob&SRS=EPSG:4326&WIDTH=676";
//		String wmsRequest = "http://thredds-d-d4s.d4science.org/thredds/wms/public/netcdf/test20.nc?service=wms&version=1.3.0&request=GetMap&layers=analyzed_field&styles=&width=640&height=480&srs=EPSG:4326&CRS=EPSG:4326&format=image/png&COLORSCALERANGE=auto&bbox=-85.0,-180.0,85.0,180.0";
		WmsUrlValidator wms;
		try {
			wms = new WmsUrlValidator(wmsRequest);
			System.out.println("Returned wms: " + wms.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*@Before
	public void fillRegioneProvinceComuniWFSRequest() {

		String wfsEndPoint = "https://geoserver1.dev.d4science.org/geoserver/devvre/wfs";
		try {
			// REGIONI
			String layerName = "devvre:Reg01012021_g_WGS84";
			List<String> propertyName = new ArrayList<String>();
			propertyName.add("devvre:COD_REG");
			propertyName.add("devvre:DEN_REG");
			// Querying list of Regioni
			WFSQueryBuilder queryBuilder = new WFSQueryBuilder();
			queryBuilder.addParameter(WFSParameter.SERVICE, WFSParameter.SERVICE.getValue());
			queryBuilder.addParameter(WFSParameter.VERSION, "2.0.0");
			queryBuilder.addParameter(WFSParameter.REQUEST, "GetFeature");
			queryBuilder.addParameter(WFSParameter.TYPENAME, layerName);
			queryBuilder.addParameter(WFSParameter.OUTPUTFORMAT, WFSParameter.OUTPUTFORMAT.getValue());
			queryBuilder.addParameter(WFSParameter.PROPERTYNAME, propertyName, ",");
			System.out.println(queryBuilder.getQuery());
			FeatureParser fp = new FeatureParser();
			List<FeatureRow> rows = fp.getWFSFeatures(wfsEndPoint, queryBuilder);
			System.out.println("Regioni: ");

			for (FeatureRow featureRow : rows) {
				System.out.println(featureRow);
				Map<String, List<String>> properties = featureRow.getMapProperties();
				tableReg.createRow(new String[] { "", properties.get("COD_REG").get(0), "Regione",
						properties.get("DEN_REG").get(0) });
			}
			System.out.println("Table regioni: " + tableReg);

			// PROVINCE
			layerName = "devvre:ProvCM01012021_g_WGS84";
			propertyName = new ArrayList<String>();
			propertyName.add("devvre:COD_REG");
			propertyName.add("devvre:COD_PROV");
			propertyName.add("devvre:DEN_PROV");
			propertyName.add("devvre:DEN_CM"); // è pieno per i capoluoghi di provincia

			queryBuilder = new WFSQueryBuilder();
			queryBuilder.addParameter(WFSParameter.SERVICE, WFSParameter.SERVICE.getValue());
			queryBuilder.addParameter(WFSParameter.VERSION, "2.0.0");
			queryBuilder.addParameter(WFSParameter.REQUEST, "GetFeature");
			queryBuilder.addParameter(WFSParameter.TYPENAME, layerName);
			queryBuilder.addParameter(WFSParameter.OUTPUTFORMAT, WFSParameter.OUTPUTFORMAT.getValue());
			queryBuilder.addParameter(WFSParameter.PROPERTYNAME, propertyName, ",");
			// queryBuilder.addParameter(WFSParameter.CQL_FILTER,
			// URLEncoder.encode("devvre:COD_REG=9", "UTF-8"));
			System.out.println(queryBuilder.getQuery());
			fp = new FeatureParser();
			rows = fp.getWFSFeatures(wfsEndPoint, queryBuilder);

			System.out.println("PROVINCE: ");
			for (FeatureRow featureRow : rows) {
				System.out.println(featureRow);
				Map<String, List<String>> properties = featureRow.getMapProperties();
				String denProv = properties.get("DEN_PROV").get(0);
				if (denProv == null || denProv.isEmpty() || denProv.equals("-")) {
					denProv = properties.get("DEN_CM").get(0); // è un capoluogo di provincia
				}

				tableProv.createRow(new String[] { properties.get("COD_REG").get(0), properties.get("COD_PROV").get(0),
						"Provincia", denProv });
			}

			// COMUNI
//			layerName = "devvre:Com01012021_g_WGS84";
//			propertyName = new ArrayList<String>();
//			//propertyName.add("devvre:COD_REG");
//			propertyName.add("devvre:COD_PROV");
//			propertyName.add("devvre:COMUNE");
//			
//			queryBuilder = new WFSQueryBuilder();
//			queryBuilder.addParameter(WFSParameter.SERVICE, WFSParameter.SERVICE.getValue());
//			queryBuilder.addParameter(WFSParameter.VERSION, "2.0.0");
//			queryBuilder.addParameter(WFSParameter.REQUEST, "GetFeature");
//			queryBuilder.addParameter(WFSParameter.TYPENAME, layerName);
//			queryBuilder.addParameter(WFSParameter.OUTPUTFORMAT, WFSParameter.OUTPUTFORMAT.getValue());
//			queryBuilder.addParameter(WFSParameter.PROPERTYNAME, propertyName, ",");
//			queryBuilder.addParameter(WFSParameter.CQL_FILTER, URLEncoder.encode("devvre:COD_PROV=9", "UTF-8"));
//			System.out.println(queryBuilder.getQuery());
//			fp = new FeatureParser();
//			rows = fp.getWFSFeatures(wfsEndPoint, queryBuilder);
//			
//			System.out.println("COMUNI: ");
//			for (FeatureRow featureRow : rows) {
//				System.out.println(featureRow);
//				Map<String, List<String>> properties = featureRow.getMapProperties();
//				String denProv = properties.get("DEN_PROV").get(0);
//				if(denProv==null || denProv.isEmpty()) {
//					denProv = properties.get("DEN_CM").get(0); //è un capoluogo di provincia
//				}
//				
//				tableProv.createRow(new String[] {properties.get("COD_REG").get(0),properties.get("COD_PROV").get(0),"Provincia",denProv});
//			}
//	

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

	/*@Test
	public void testQueryTableRegioneProvince() {

		try {

			Table<Integer, Column, String> theTableProv = tableReg.table();
			List<Location> locations = new ArrayList<Location>();
			for (Integer tableRowId : theTableProv.rowKeySet()) {
				Location location = rowToLocation(theTableProv, tableRowId);
				locations.add(location);
			}

			System.out.println("Location Regioni: " + locations);

			String regID = "9";
			EqualPredicate equalPredicate = new EqualPredicate(Column.PARENT_LOCATION_ID, regID);
			EnumSet<Column> returnColumn = EnumSet.of(Column.PARENT_LOCATION_ID, Column.LOCATION_ID,
					Column.LOCATION_TYPE, Column.LOCATION_DEN);
			
			Table<Integer, Column, String> tableResult = tableProv.query(equalPredicate, returnColumn);

			List<Location> locationResult = new ArrayList<Location>();
			for (Integer rowId : tableResult.rowKeySet()) {
				Location location = rowToLocation(tableResult, rowId);
				locationResult.add(location);
			}

			System.out.println("Query location for " + equalPredicate + ":");
			System.out.println(locationResult);

//			
//			String valueType = "Regione";
//			String regId = "9"; //Toscana
//			Stream<Map<Column, String>> stream1 = tableReg.table().rowMap().values().stream().filter(row -> {
//			    return row.get(Column.LOCATION_TYPE) != null && row.get(Column.LOCATION_ID) !=null && row.get(Column.LOCATION_TYPE).equals(valueType)  && row.get(Column.LOCATION_ID).equals(regId);
//			});
//			
//			EqualPredicate equalPredicate = new EqualPredicate(Column.LOCATION_ID, regId);
//			stream1.forEach(s -> System.out.println(s));
//            // Print the stream
//            System.out.println("Query regione:" +tableReg.query(equalPredicate,returnColumn));
//
//	        CustomerTable tableProv = new CustomerTable();
//			
//			//PROVINCE
//			layerName = "devvre:ProvCM01012021_g_WGS84";
//			propertyName = new ArrayList<String>();
//			propertyName.add("devvre:COD_REG");
//			propertyName.add("devvre:COD_PROV");
//			propertyName.add("devvre:DEN_PROV");
//			propertyName.add("devvre:DEN_CM"); //è pieno per i capoluoghi di provincia
//			
//			queryBuilder = new WFSQueryBuilder();
//			queryBuilder.addParameter(WFSParameter.SERVICE, WFSParameter.SERVICE.getValue());
//			queryBuilder.addParameter(WFSParameter.VERSION, "2.0.0");
//			queryBuilder.addParameter(WFSParameter.REQUEST, "GetFeature");
//			queryBuilder.addParameter(WFSParameter.TYPENAME, layerName);
//			queryBuilder.addParameter(WFSParameter.OUTPUTFORMAT, WFSParameter.OUTPUTFORMAT.getValue());
//			queryBuilder.addParameter(WFSParameter.PROPERTYNAME, propertyName, ",");
//			//queryBuilder.addParameter(WFSParameter.CQL_FILTER, URLEncoder.encode("devvre:COD_REG=9", "UTF-8"));
//			fp = new FeatureParser();
//			rows = fp.getWFSFeatures(wfsEndPoint, queryBuilder);
//			System.out.println("PROVINCE: ");
//			for (FeatureRow featureRow : rows) {
//				//System.out.println(featureRow);
//				Map<String, List<String>> properties = featureRow.getMapProperties();
//				String denProv = properties.get("DEN_PROV").get(0);
//				if(denProv==null || denProv.isEmpty()) {
//					denProv = properties.get("DEN_CM").get(0); //è un capoluogo di provincia
//				}
//				
//				tableProv.createRow(new String[] {properties.get("COD_REG").get(0),properties.get("COD_PROV").get(0),"Provincia",denProv});
//			}
//
//			System.out.println("Table PROVINCE: "+tableProv);
//			
//			Table<Integer, Column, String> theTableProv = tableProv.table();
//			List<Location> locationsProv = new ArrayList<Location>();
//			for (Integer tableRowId : theTableProv.rowKeySet()) {
//				Location location = rowToLocation(table, tableRowId);
//				locationsProv.add(location);
//			}
//			System.out.println("Location Province: "+locationsProv);
//	
//			
//			EqualPredicate ep = new EqualPredicate(Column.PARENT_LOCATION_ID,"5");
//			Table<Integer, Column, String> provQuery = tableProv.query(ep,  returnColumn);
//			System.out.println("Query PROVINCIA: "+provQuery);
//			
//
//			System.out.println(queryBuilder.getQuery());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}*/

	/*
	public static Location rowToLocation(Table<Integer, Column, String> table, Integer rowId) {
		Map<Column, String> column = table.row(rowId);
		column.get(Column.PARENT_LOCATION_ID);
		column.get(Column.LOCATION_ID);
		column.get(Column.LOCATION_TYPE);
		column.get(Column.LOCATION_DEN);
		return new Location(column.get(Column.PARENT_LOCATION_ID), column.get(Column.LOCATION_ID),
				column.get(Column.LOCATION_TYPE), column.get(Column.LOCATION_DEN));
	}*/
}
