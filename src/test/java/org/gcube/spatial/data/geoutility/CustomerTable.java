//package org.gcube.spatial.data.geoutility;
//
//import java.util.EnumSet;
//import java.util.Map;
//import java.util.stream.Stream;
//
//import org.gcube.spatial.data.geoutility.CustomerTable.Column;
//
//import com.google.common.base.Predicate;
//import com.google.common.collect.HashBasedTable;
//import com.google.common.collect.Maps;
//import com.google.common.collect.Table;
//
//public class CustomerTable {
//
//	public enum Column {
//		PARENT_LOCATION_ID, LOCATION_ID, LOCATION_TYPE, LOCATION_DEN;
//	}
//
//	private Table<Integer, Column, String> table = HashBasedTable.create();
//
//	@Override
//	public String toString() {
//		return table.toString();
//	}
//
//	public void createRow(String[] values) {
//		if (Column.values().length != values.length) {
//			throw new IllegalArgumentException();
//		}
//		Integer rowNum = table.rowKeySet().size() + 1;
//		for (int i = 0; i < values.length; i++) {
//			table.put(rowNum, Column.values()[i], values[i]);
//		}
//	}
//
////    private void query() {
////		// TODO Auto-generated method stub
////
////	}
////
////    public Table<Integer, Column, String> query(Predicate<Map<Column, String>> query) {
////        return query(query, allOf(Column.class));
////    }
//
//	public Table<Integer, Column, String> query(Predicate<Map<Column, String>> query, EnumSet<Column> columns) {
//		Map<Integer, Map<Column, String>> filtered = Maps.filterValues(table.rowMap(), query);
//		return createResultTable(filtered, columns);
//	}
//
//	private Table<Integer, Column, String> createResultTable(Map<Integer, Map<Column, String>> resultMap,
//			final EnumSet<Column> columns) {
//
//		int i = 0;
//		Table<Integer, Column, String> result = HashBasedTable.create();
//		for (Map<Column, String> row : resultMap.values()) {
//			i++;
//			for (Column column : row.keySet()) {
//				if (columns.contains(column)) {
//					result.put(i, column, row.get(column));
//				}
//			}
//		}
//		return result;
//	}
//
//	public static void main(String[] args) {
//
//	}
//
//	public Stream<Map<Column, String>> filter(String column, String value) {
//		Stream<Map<Column, String>> stream = table.rowMap().values().stream().filter(row -> {
//			return row.get(column).contains(value);
//		});
//		return stream;
//	}
//
//	public Table<Integer, Column, String> table() {
//
//		return table;
//	}
//}
//
//class EqualPredicate implements Predicate<Map<CustomerTable.Column, String>> {
//
//	private Column column;
//	private String value;
//
//	public EqualPredicate(Column column, String value) {
//		this.column = column;
//		this.value = value;
//	}
//
//	@Override
//	public boolean apply(Map<Column, String> input) {
//		return input.get(column) != null && input.get(column).equals(value);
//
//	}
//
//	public static EqualPredicate equal(Column column, String value) {
//		return new EqualPredicate(column, value);
//	}
//
//	@Override
//	public String toString() {
//		StringBuilder builder = new StringBuilder();
//		builder.append("EqualPredicate [column=");
//		builder.append(column);
//		builder.append(", value=");
//		builder.append(value);
//		builder.append("]");
//		return builder.toString();
//	}
//
//	
//}
//
//
//class Location {
//
//	String parentId;
//	String id;
//	String type;
//	String name; // is denominazione
//
//	public Location(String parentId, String id, String type, String name) {
//		super();
//		this.parentId = parentId;
//		this.id = id;
//		this.type = type;
//		this.name = name;
//	}
//
//	public String getParentId() {
//		return parentId;
//	}
//
//	public String getId() {
//		return id;
//	}
//
//	public String getType() {
//		return type;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	@Override
//	public String toString() {
//		StringBuilder builder = new StringBuilder();
//		builder.append("Location [parentId=");
//		builder.append(parentId);
//		builder.append(", id=");
//		builder.append(id);
//		builder.append(", type=");
//		builder.append(type);
//		builder.append(", name=");
//		builder.append(name);
//		builder.append("]");
//		return builder.toString();
//	}
//
//}