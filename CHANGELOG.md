
# Changelog for geo-utility

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1-3-0]

- Improved WFS builder [#26026]

## [v1.2.0] - 2022-02-01

- [#22747] Added WFS utilities

## [v1.1.2] - 2019-10-24

- [#17831#change-92733] Bug fixed


## [v1.1.1] - 2016-04-27

- Updated comment in WMSGetStyles class
- Bug fixing for method getValueOfParameter


## [v1.1.0] - 2016-02-09

- [Feature #2191] Updated NcWmsGetMetadata to retrieve z-axis


## [v1.0.0] - 2016-01-26

- [#2054] First release
